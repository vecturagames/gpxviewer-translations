﻿----------
| Title: |
----------
GPX Viewer - Trase, Rute i Prolazne tačke



-------------------------------------------------------------------------------------
| Short description (maximum allowed length in Google Play Store is 80 characters): |
-------------------------------------------------------------------------------------
GPX Viewer prikazuje trase, rute i prolazne tačke iz gpx i kml datoteka.



----------------
| Description: |
----------------
Pregledajte gpx, kml, kmz, loc datoteke, i dobite puno više funkcija. Saznajte zašto je naša aplikacija jedna od najbolje ocenjenih aplikacija za pregled i crtanje mapa. GPX Viewer je GPS ultimativni lokator, prikazivač i analizator GPS trasa, rekorder trasa i jednostavan navigacioni alat za aktivnosti u prirodi ili putovanje. 

<b>GPX, KML, KMZ I LOC</b>
• Pregledač trasa, ruta i prolaznih tačaka iz gpx, kml, kmz i loc datoteka
• Pregledač datoteka koji omogućuje otvoriti više datoteka istovremeno i podržava omiljene datoteke i istoriju
• Kompresija gpx datoteka u gpz i kml datoteka u kmz (zip arhivi)

<b>DETALJNE STATISTIKE</b>
• Analizirajte informacije i statistike o trasama i rutama
• Pregledajte grafikone kao što su profil nadmorske visine i profil brzine za trase i rute
• Pregledajte grafikone i za ostale podatke kao što su kadenca, otkucaje srca, snaga i temperatura vazduha
• Analizirajte informacije prolaznih tačaka i prilagodite njihove ikone
• Promenite boje za trase i rute 
• Mogućnost obojiti crte za trase i rute prema profilu, brzini, kadenci, otkucajima srca, snazi i temperaturi vazduha

<b>ONLINE MAPE</b>
• Online mape kao Google mape, Mapbox, HERE, Thunderforest i ostale bazirane na OpenStreetMap podatcima, kao primer: https://go.vecturagames.com/online (Mapbox, HERE i mrežne mape Thunderforest potrebno je kupiti)
• OpenWeatherMap slojevi mapa i slojevi trenutnog vremena (treba da se kupi)
• Dodajte vlastite online TMS i WMS mape

<b>JEDNOSTAVAN NAVIGACIONI ALAT</b>
• Prikazuje trenutnu GPS poziciju na mapi
• Pomakne mapu da bi prikazivala trenutnu GPS poziciju
• Rotira mapu prema senzoru orijentacije ili prema podatcima o smeru kretanja iz GPS
• S funkcijama kao što su praćenje GPS pozicije i rotiranje mape, možete GPX Viewer koristiti kao jednostavan navigacioni alat
• Obaveštava kad se GPS pozicija približi blizu prolazne tačke čiju udaljenost je moguće podesiti

<b>INTEGRACIJA TRACKBOOK</b>
• Sinhronizovati trase i prolazne tačke kreirane na Trackbook - https://trackbook.online

---------

GPX Viewer možete prilagoditi u skladu s vašim vlastitim potrebama. Sve je moguće podesiti tako da vam potpuno zadovoljava!

Ako želite pregledač gpx datoteka s mnogo funkcija koji sadržava online mape s jednostavnom navigacijom i GPS lokator, prikazuje GPS trase, statistike trasa i ima još ostale korisne funkcije, GPX Viewer najbolja je aplikacija za vas!



----------------------------
| Screenshot descriptions: |
----------------------------
1. Prikazuje trase, rute, i prolazne tačke iz gpx i kml datoteka
2. Prikazuje trase i rute
3. Analiza trasa
4. Prikazuje prolazne tačke
5. Puno tipova karata, OpenWeatherMap i WMS mape
6. Informacija i statistika o trasama, rutama i prolaznim tačkama
7. Grafikoni uspona, brzine, ritma, otkucaja srca i snage
8. Pregledač datoteka kojim se mogu otvoriti jedna ili više datoteka
