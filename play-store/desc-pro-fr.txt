﻿----------
| Title: |
----------
GPX Viewer PRO - Pistes, routes et points



-------------------------------------------------------------------------------------
| Short description (maximum allowed length in Google Play Store is 80 characters): |
-------------------------------------------------------------------------------------
GPX Viewer PRO affiche les pistes, routes et points depuis des fichiers gpx, kml



----------------
| Description: |
----------------
Affichez les fichiers gpx, kml, kmz, loc, et obtenez encore plus de fonctionnalités. Découvrez pourquoi nous sommes l’une des apps de cartes vectorielles offline les mieux notées. GPX Viewer PRO est le traceur GPS ultime: il vous permet de visualiser les pistes GPS, d’analyser, enregistrer, suivre en temps réel. Vous bénéficiez d’un outil de navigation simple et complet pour vos voyages et vos activités de plein air.

<b>GPX, KML, KMZ AND LOC</b>
• Visualisez chemins, itinéraires et points de repère dans des fichiers gpx, kml, kmz et loc 
• Bénéficiez d’un navigateur qui ouvre plusieurs fichiers et prend en charge l’historique et les favoris
• Compressez les fichiers gpx files en gpz et le kml en kmz (archives zip)

<b>DES STATISTIQUES DE VOYAGE DETAILLEES</b>
• Analyse de l’information et des statistiques des itinéraires et chemins
• Obtenez des graphiques de données tels que l’altitude ou la vitesse sur vos itinéraires et chemins
• Obtenez des graphiques et autres données tels que la cadence, le rythme cardiaque, la température extérieure
• Analysez les informations pour les points de repère et ajustez les icônes
• Modifiez les couleurs des pistes et itinéraires
• Colorez les chemins et itinéraires en fonction de l’altitude, la vitesse, la cadence, le rythme cardiaque et la température extérieure

<b>DES CARTES EN LIGNE</b>
• Des cartes connectées telles que Google Maps, Mapbox, HERE, Thunderforest et d’autres, tirées de l’OpenStreetMap data, prévisualisation: https://go.vecturagames.com/online
• OpenWeatherMap pour les calques de couleurs en fonction des conditions météorologiques
• Personnalisez en ligne les cartes TMS or WMS

<b>UN OUTIL DE NAVIGATION SIMPLE</b>
• Montre votre situation GPS sur une carte
• Suit votre position GPS de façon continue en ajustant la position de la carte sur l’écran
• Pivote la carte en fonction de votre smartphone ou tablette ou de la direction des données du GPS
• Avec ce suivi GPS et les fonctionnalités de pivot de la carte, GPX Viewer PRO peut être utilisé comme un simple outil de navigation
• Notification envoyées quand la position GPS est proche du point de repère avec une distance ajustable

<b>INTÉGRATION DU TRACKBOOK</b>
• Synchroniser les traces et les points de cheminement créés sur le Trackbook - https://trackbook.online

<b>CARTES OFFLINE (UNIQUEMENT EN VERSION PRO)</b>
• Des cartes vectorielles du monde entier basées sur OpenStreetMap data
• Une large variété de styles de cartes: plan de ville ou piste en pleine nature prévisualisation: https://go.vecturagames.com/offline
• Des mises à jour mensuelles et des données toujours améliorées

<b>CRÉER & MODIFIER (UNIQUEMENT EN VERSION PRO)</b>
• Créez de nouvelles pistes ou modifiez des pistes et des routes existantes
• Divisez une piste ou route en deux
• Fusionnez deux pistes ou routes en une seule
• Ajoutez des points de cheminement sur la carte et définissez leur nom et icône

<b>ENREGISTREMENT DES PARCOURS (UNIQUEMENT EN VERSION PRO)</b>
• Enregistre et exportes vos parcours en fichiers gpx or kml
• Enregistre les données d’altitude et de vitesse
• Des profils d’enregistrement qui s’ajustent en fonction de vos activités extérieures
• Des notifications vocales de distance et de durée

<b>PRÉVISION MÉTÉO (UNIQUEMENT EN VERSION PRO)</b>
• Prévisions météo pour les 7 prochains jours
• Afficher les prévisions horaires

---------

GPX Viewer PRO est finement paramétrable. Vous pouvez l’ajuster en fonction de vos besoins!

Si vous souhaitez des fonctionnalités poussées, des cartes disponibles hors connexion, des outils de navigation simples et performants, des cartes vectorielles disponibles hors ligne, un traceur GPS, un GPS pour les pistes, des statistiques de parcours et d’autres fonctionnalités utiles GPX Viewer PRO est la meilleure app pour vous!



----------------------------
| Screenshot descriptions: |
----------------------------
1. Visualisez les pistes, routes et points depuis des fichiers gpx et kml
2. Carte vectorielle hors-ligne basée sur OpenStreetMap
3. Enregistrement du parcours
4. Créer et modifier des pistes et des points de cheminement
5. Analyse de piste
6. Ajouter et personnaliser points de cheminements
7. De nombreux types de cartes, OpenWeatherMap, cartes WMS personnalisées
8. Des graphes d’élévation, vitesse, cadence, rythme cardiaque et puissance
